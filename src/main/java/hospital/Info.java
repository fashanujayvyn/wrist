/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospital;

/**
 *
 * @author jayvy
 */
public class Info {

private int age;
private String gender;
private String medical_history;


public Info(int age, String gender, String medical_history){
    
 this.age=age;
 this.gender=gender;
 this.medical_history=medical_history;
}

public int getAge(){
    
 return age;   
}

public String getGender(){
    
 return gender;   
    
}

public String getMedical_history(){
    
 return medical_history;   
}

public void setAge(int age){
 this.age=age;   
    
}

public void setGender(String gender){
    this.gender=gender;
    
}

public void setMedical_history(String medical_history){
    
 
    this.medical_history=medical_history;
}
 

}
